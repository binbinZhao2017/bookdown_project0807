--- 
title: "GV009 project analysis"
author: "GV-RI"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "This is a analysis report for GV009 projects."
tables: true
---

# Introduction

This analysis focus on 4 selected project from total GV-009 study :  **ONC-2019-LZ-023**, **ONC-2020-003** , **ONC-2020-010**, **ONC-2020-013**. 

```{r include=FALSE}
# automatically create a bib database for R packages
knitr::write_bib(c(
  .packages(), 'bookdown', 'knitr', 'rmarkdown'
), 'packages.bib')
```

```{r fig.align='center',fig.width=30,fig.height=30, echo=F}
knitr::include_graphics("data/09_07_2020/overall_table_4projects_21_07_2020.png",dpi = 25)

```
